<?php
namespace App;

use Picco\Container;
use Picco\App;

class Mock { public static $routing; public static $services; }
class Routing { public static function get() { return Mock::$routing; } }
class Services { public static function get(Container $c) { $cb = Mock::$services; $cb($c); } }
class Controllers {
    public function foo() { return ['foo' => 8, 'bar' => 69]; }
    public function error(Container $c, \Exception $e) { return ['message' => $e->getMessage()]; }
}

$_SERVER = [
    'PATH_INFO' => '/test/',
    'REQUEST_URI' => '/base/app_dev.php/test/?foo=bar',
];

$a = new App(__DIR__ . '/help/web/../',false);

a(session_id());
a(D === __DIR__ . '/help/web/../');
a(F === '/base/app_dev.php');
a(B === '/base/');
a(R === '/test');
a(E === false);

Mock::$routing = [ 'foo' => '/test' ];
Mock::$services = function(Container $c) { };
$expected = <<<HTML
<p>foo = 8</p>
<p>view = foo</p>
<p>vars = {"foo":8,"bar":69,"r":{}}</p>
<p>bar = 69</p>
<p>view = bar</p>
<p>vars = {"foo":8,"bar":69,"r":{}}</p>
HTML;
a($a->run() === $expected . "\n");

Mock::$routing = [];
Mock::$services = function(Container $c) { };
a($a->run() === "<p>message = /test not found</p>\n");

Mock::$routing = [ 'foo' => '/test' ];
Mock::$services = function(Container $c) { $c->dispatcher->request = function() { return 'blocked'; }; };
a($a->run() === 'blocked');

Mock::$routing = [ 'foo' => '/test' ];
Mock::$services = function(Container $c) { $c->dispatcher->response = function(Container $c, \stdClass $vars) { $vars->foo = 'overwritten'; }; };
$expected = <<<HTML
<p>foo = overwritten</p>
<p>view = foo</p>
<p>vars = {"foo":"overwritten","bar":69,"r":{}}</p>
<p>bar = 69</p>
<p>view = bar</p>
<p>vars = {"foo":"overwritten","bar":69,"r":{}}</p>
HTML;
a($a->run() === $expected . "\n");

Mock::$routing = [];
Mock::$services = function(Container $c) { $c->dispatcher->error = function(Container $c, \Exception $e) { return $e->getMessage(); }; };
a($a->run() === "/test not found");
