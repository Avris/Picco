<?php

$r = new Picco\Router([
    'home' => '',
    'itemList' => '/item/list',
    'itemShow' => '/item/(\d+)/show/?(.*)',
    'changeLocale' => '/locale/([A-Z]{2})'
]);

function assertRouteMatch($url, $controller, $params = []) {
    global $r;
    a([$controller, array_merge([$url], $params)] === $r->match($url));
}

assertRouteMatch('', 'home');
assertRouteMatch('/item/list', 'itemList');
assertRouteMatch('/item/12/show', 'itemShow', ['12', '']);
assertRouteMatch('/item/2/show', 'itemShow', ['2', '']);
assertRouteMatch('/item/7/show/abc', 'itemShow', ['7', 'abc']);
assertRouteMatch('/locale/DE', 'changeLocale', ['DE']);

function assertRouteNotFound($url) {
    global $r;
    try {
        $r->match($url);
        a(false);
    } catch (Exception $e) {
        a($e->getMessage() === "$url not found");
        a($e->getCode() === 404);
    }
}

assertRouteNotFound('/foo');
assertRouteNotFound('/item//show');
assertRouteNotFound('/locale/pl');

a($r->get('home') == F.'/');
a($r->get('itemList') == F.'/item/list');
a($r->get('itemShow', [10]) == F.'/item/10/show');
a($r->get('itemShow', [10, 'foo']) == F.'/item/10/show/foo');
a($r->get('changeLocale', ['EN']) == F.'/locale/EN');

a(@$r->get('xxx', ['EN']) == F.'/');