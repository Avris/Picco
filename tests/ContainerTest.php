<?php

class FooService {
    public static $count = 0;
    function __construct() {
        self::$count++;
    }
}

class BarService {
    public $foo, $lorem;
    function __construct($foo, $lorem) {
        $this->foo = $foo;
        $this->lorem = $lorem;
    }
}


$c = new Picco\Container();

$c->lorem = 'ipsum';
a('ipsum' == $c->lorem);

$c->foo = function() { return new FooService(); };

a(FooService::$count === 0);
a($c->foo instanceof FooService);
a(FooService::$count === 1);
a($c->foo instanceof FooService);
a($c->foo === $c->foo);
a(FooService::$count === 1);

$c->anotherFoo = new FooService();

a(FooService::$count === 2);
a($c->anotherFoo instanceof FooService);
a($c->anotherFoo instanceof FooService);
a($c->anotherFoo === $c->anotherFoo);
a(FooService::$count === 2);

a($c->foo !== $c->anotherFoo);

$c->bar = function($c) { return new BarService($c->foo, $c->lorem); };

a($c->bar instanceof BarService);
a($c->bar->foo instanceof FooService);
a($c->bar->foo === $c->foo);
a($c->lorem == 'ipsum');
