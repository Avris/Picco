<?php

$v = new Picco\View;

$expected = <<<HTML
<p>foo = 8</p>
<p>view = foo</p>
<p>vars = {"foo":8,"bar":69}</p>
<p>bar = 69</p>
<p>view = bar</p>
<p>vars = {"foo":8,"bar":69}</p>
HTML;

$actual = $v->render('foo', ['foo' => 8, 'bar' => 69]);

a($actual === $expected . "\n");