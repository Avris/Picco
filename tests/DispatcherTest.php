<?php

$d = new Picco\Dispatcher;

$out = 0;

$d->foo = function($a, $b) use (&$out) { $out += $a + $b; if ($a == 0) { return true; } };

$d->foo(1,2);
a(3 == $out);

$d->foo(3,4);
a(10 == $out);

$d->foo(0,1);
a(11 == $out);

$d->foo = function($a, $b) use (&$out) { $out += 2*($a + $b); };

$d->foo(1,2);
a(20 == $out); // current 11 + first listener 3 + second listener 6

$d->foo(0,4);
a(24 == $out); // current 20 + first listener 4, second listener not executed
