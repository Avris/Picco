<?namespace Picco;class App{
function __construct($d,$e){session_start();define('D',$d);$u=explode('?',($s=$_SERVER)['REQUEST_URI'])[0];$l=strlen($r=@$s['PATH_INFO'])?:$u=='/';define('F',$l?substr($u,0,-$l):$u);define('R',rtrim($r,'/')?:'/');define('B',preg_replace('#/[^/]+\.php$#','',F).'/');define('E',$e);}
function run(){$c=new Container();$c->router=new Router(\App\Routing::get());$c->view=new View;$q=$c->controllers=new\App\Controllers;$d=$c->dispatcher=new Dispatcher;\App\Services::get($c);
$t=function($f,$m)use($c,$q,$d){$r=(object)call_user_func_array([$q,$f],$m);return$d->response($c,$r)?:$c->view->render($f,(array)$r+['r'=>$c->router]);};
$d->error=function($c,$e)use($t){if(E)throw$e;return$t('error',[$c,$e]);};
set_error_handler(function($t,$m,$f,$l)use($c,$d){if(error_reporting()){echo$d->error($c,new\ErrorException("$m in $f line $l",500));die;}},E_ALL);
try{list($f,$m)=$c->router->match(R);return$d->request($c,$f,$m)?:$t($f,[0=>$c]+$m);}catch(\Exception$e){return$d->error($c,$e);}}}